clear; close all; clc

%% preprocessing
data = readtable('past_isotope.csv');
now = datetime('2021-01-01');
date = datetime(data.date);
duration = years(now - date)'; % years before Jan 1st 2021

%% primary data
o18 = flip(data.dO18');
c13 = flip(data.dC13');
t0 = flip(duration);

%% setup interval
tmax = max(t0); % 277.7525 yr BP
tmin = min(t0); % 172.0857 yr BP
s = size(t0, 2);
interval = (tmax - tmin) / s; % 0.2577 yrs
t = tmin:interval:tmax;

%% O18 interpolation
linO18 = interp1(t0, o18, t, 'linear');
cubO18 = interp1(t0, o18, t, 'spline');
phcO18 = interp1(t0, o18, t, 'phcip');

figure(1)
plot(t0, o18, 'ko'), hold on
plot(t, linO18, 'b-', t, cubO18, 'r-',t, phcO18, 'g-')
legend('Original Points','Linear', 'Cubic Spline', 'Piecewise Cubic Hermite', 'Location', 'southwest')
grid on
xlabel('years BP')
ylabel('\delta^{18}O (permil)')
title('Past \delta^{18}O interpolation')
hold off

%% C13 interpolation
linC13 = interp1(t0, c13, t, 'linear');
cubC13 = interp1(t0, c13, t, 'spline');
phcC13 = interp1(t0, c13, t, 'phcip');

figure(2)
plot(t0, c13, 'ko'), hold on
plot(t, linC13, 'b-', t, cubC13, 'r-',t, phcC13, 'g-')
legend('Original Points','Linear', 'Cubic Spline', 'Piecewise Cubic Hermite', 'Location', 'southwest')
grid on
xlabel('years BP')
ylabel('\delta^{13}C (permil)')
title('Past \delta^{13}C interpolation')
hold off

%% save datasets
linear = table(t', linO18', linC13');
cubic = table(t', cubO18', cubC13'); 
phcip = table(t', phcO18', phcC13');

writetable(linear, 'pastLin.csv', 'Delimiter', ',')
writetable(cubic, 'pastCub.csv', 'Delimiter', ',')
writetable(phcip, 'pastPHC.csv', 'Delimiter', ',')