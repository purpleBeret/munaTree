clear; close all; clc

% Past
data = readtable("past_isotope.csv");
ori_time_p = dateshift(data.date, 'start', 'month');
tmax = max(ori_time_p);
tmin = min(ori_time_p);
tpinterp = tmin:calmonths(3):tmax;

past_oritn = datenum(ori_time_p);
past_tn = datenum(tpinterp);


% Modern
data = readtable("modern_isotope.csv");
ori_time_m = dateshift(data.date, 'start', 'month');
tmax = max(ori_time_m);
tmin = min(ori_time_m);
tminterp = tmin:calmonths(3):tmax;

mod_oritn = datenum(ori_time_m);
mod_tn = datenum(tminterp);

clear data tmin tmax

save('time_process')

writematrix(tminterp', 'tminterp.csv', 'Delimiter', 'comma');
type tminterp.csv

writematrix(tpinterp', 'tpinterp.csv', 'Delimiter', 'comma');
type tpinterp.csv