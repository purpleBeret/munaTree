"""
Trimonthly Isotope Data Reconstruction 
Using Linear Interpolation

Sandy H. <herho@terpmail.umd.edu>
01/07/21
"""

# I split the data into two periods:
# Past: Apr 1743 - Oct 1848
# Modern: Jul 1956 - Jul 1995

# import libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.io import loadmat
from scipy.interpolate import interp1d, UnivariateSpline
from matplotlib.ticker import FixedLocator, FixedFormatter
plt.style.use('ggplot')

# load time dataset
t = loadmat('time_process.mat')
tp = pd.read_csv('tpinterp.csv', header=None).to_numpy().flatten()
tm = pd.read_csv('tminterp.csv', header=None).to_numpy().flatten()

# PAST
t_ori = t['past_oritn'].flatten()
t_interp = t['past_tn'].flatten()

c13 = pd.read_csv("past_isotope.csv")['dC13'].to_numpy().flatten()
o18 = pd.read_csv("past_isotope.csv")['dO18'].to_numpy().flatten()

## dO18 fig
ox = t_ori; oy = o18; oxnew = t_interp
f1 = interp1d(ox, oy) # linear interpolation
f2 = UnivariateSpline(x,y, k=2) # univariate spline with k = 2

fig = plt.figure(figsize=(20,10));
ax = fig.gca();
ax.plot(ox,oy, 'o', oxnew, f1(oxnew), '-', oxnew, f2(oxnew), 'x-');
ax.legend(['data', 'linear', 'univariate spline (k=2)'], loc='best');
ax.set_ylabel('$\delta^{18}O$ (permil)');
x_formatter = FixedFormatter(['xxx','Jul 1738', 'Apr 1752', 'Dec 1765', 'Aug 1779','Apr 1793',
                              'Jul 1807', 'Sep 1820', 'May 1834', 'Feb 1848'])
ax.xaxis.set_major_formatter(x_formatter);
ax.set_xlabel('Date');
plt.savefig('../figs/dO18past.png')

## dC13 fig
cx = t_ori; cy = c13; cxnew = t_interp
f1 = interp1d(cx, cy) # linear interpolation
f2 = UnivariateSpline(x,y, k=2) # univariate spline with k = 2

fig = plt.figure(figsize=(20,10));
ax = fig.gca();
ax.plot(cx,cy, 'o', cxnew, f1(cxnew), '-', cxnew, f2(cxnew), 'x-');
ax.legend(['data', 'linear', 'univariate spline (k=2)'], loc='best');
ax.set_ylabel('$\delta^{13}C$ (permil)');
ax.xaxis.set_major_formatter(x_formatter);
ax.set_xlabel('Date');
plt.savefig('../figs/dC13past.png')

## save data
do18p = f1(oxnew)
dc13p = f1(cxnew)
df = pd.DataFrame(data = {'dO18' : do18p, 'dC13':dc13p}, index = tp)
df.index.name = 'Date'
df.to_csv('../linearInterpIsodata/pastIsoLinterp.csv')

# MODERN
t_ori = t['mod_oritn'].flatten()
t_interp = t['mod_tn'].flatten()

c13 = pd.read_csv("modern_isotope.csv")['dC13'].to_numpy().flatten()
o18 = pd.read_csv("modern_isotope.csv")['dO18'].to_numpy().flatten()

## dO18 fig
ox = t_ori; oy = o18; oxnew = t_interp
f1 = interp1d(ox, oy) # linear interpolation
f2 = UnivariateSpline(ox,oy, k=3) # univariate spline with k = 2

fig = plt.figure(figsize=(20,10));
ax = fig.gca();
ax.plot(ox,oy, 'o', oxnew, f1(oxnew), '-', oxnew, f2(oxnew), 'x-');
ax.legend(['data', 'linear', 'univariate spline (k=3)'], loc='best');
ax.set_ylabel('$\delta^{18}O$ (permil)');
x_formatter = FixedFormatter(['xxx','Nov 1954', 'May 1960', 'Oct 1965', 'Apr 1971','Oct 1976',
                              'Mar 1982', 'Sep 1987', 'Mar 1993'])
ax.xaxis.set_major_formatter(x_formatter);
ax.set_xlabel('Date');
plt.savefig('../figs/dO18mod.png')

## dC13 fig
cx = t_ori; cy = c13; cxnew = t_interp
f1 = interp1d(cx, cy) # linear interpolation
f2 = UnivariateSpline(cx,cy, k=3) # univariate spline with k = 2

fig = plt.figure(figsize=(20,10));
ax = fig.gca();
ax.plot(cx,cy, 'o', cxnew, f1(cxnew), '-', cxnew, f2(cxnew), 'x-');
ax.legend(['data', 'linear', 'univariate spline (k=3)'], loc='best');
ax.set_ylabel('$\delta^{18}C$ (permil)');
ax.xaxis.set_major_formatter(x_formatter);
ax.set_xlabel('Date');
plt.savefig('../figs/dC13mod.png')

## save data
do18m = f1(oxnew)
dc13m = f1(cxnew)
df = pd.DataFrame(data = {'dO18' : do18m, 'dC13':dc13m}, index = tp)
df.index.name = 'Date'
df.to_csv('../linearInterpIsodata/modIsoLinterp.csv')
