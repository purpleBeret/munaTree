"""
Convert Decimal Year Data to Clean Unevenly-Spaced
Monthly Data

Sandy H. <herho@terpmail.umd.edu>
01/06/21
"""
import pandas as pd
from PyAstronomy import pyasl

df = pd.read_csv("raw_data.csv")

dt = df['date'].apply(pyasl.decimalYearGregorianDate)
dt = dt.apply(lambda x : x.date())

c13 = df['c13'].to_numpy();o18 = df['o18'].to_numpy()

df = pd.DataFrame({'dC13':c13, 'dO18':o18},index=dt)
df = df.groupby('date').mean().reset_index()
df.to_csv("data_clean.csv", index=False)

modern = df.loc[412:]
past = df.loc[:409]

modern.to_csv("modern_isotope.csv", index=False)
past.to_csv('past_isotope.csv', index=False)
