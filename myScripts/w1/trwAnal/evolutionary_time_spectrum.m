%{
TRW Evolutionary Time-Spectrum Analysis
Sandy. H <herho@terpmail.umd.edu>
12/09/2020
%}

clear; close all; clc
trw = normalize(detrend(readtable('.\data\tg01c_trws.csv').Var2'));

spectrogram(trw)
xlabel('Frequency (yr^{-1})')
ylabel('Year')
set(gca, 'YTickLabel', {'1974','1954', ...,
    '1934', '1914', '1894', ...
    '1874', '1854', '1834', ...
    '1814', '1794', '1774'})
colormap(jet)
colorbar
% colormapeditor
