%{
Simple Recurrence Analysis for TRW tg01c
Sandy. H <herho@terpmail.umd.edu>
12/13/2020
 %}

clear; close all; clc

data = readtable('.\data\tg01c_trws.csv');
trw = data.Var2';
t = 1:length(trw);


N = length(trw);

S = zeros(N,N);

for i = 1 : N
    S(:, i) = abs(repmat(trw(i), N, 1) - trw(:));
end

imagesc(t,t,S < 1)
colormap([1 1 1; 0 0 0])
colorbar
xlabel('Time'), ylabel('Time')
axis xy