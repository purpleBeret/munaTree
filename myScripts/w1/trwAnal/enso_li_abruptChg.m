%{
Detecting Abrupt Changes in 
ENSO Index using Non-parametric statistics

Sandy H. <herho@terpmail.umd.edu>
12/10/2020
%}

clear all; close all; clc

%% Define data
y = readtable('.\data\enso_li.csv').ENSO';
t = 1:length(y);


%% Paired-sliding window tests
% 2 samples of 35 data points,
% 2 samples of 70 data points,
% 2 samples of 140 data points
w = [70 140 280];

%% Mann-Whitney U-Test

for j = 1:length(w)
    na = w(j);
    nb = w(j);
    for i = w(j) / 2 + 1 : length(y) - w(j) / 2
        [p, h] = ranksum(y(i - w(j)/2 : i - 1), ...
            y(i + 1:i+w(j)/2));
        mwreal(j,i) = p;
    end
    mwreal(j, 1:w(j) / 2) = mwreal(j, w(j)/2 + 1) * ones(1,w(j)/2);
    mwreal(j, length(y) - w(j)/2 + 1 : length(y)) = ...
        mwreal(j, length(y) - w(j)/2) * ones(1,w(j)/2);
end


subplot(2,1,1)
plot(t, y), grid
title('Tree-Ring Based Reconstructed ENSO Index (Li et al. (2011))')
set(gca, 'XTickLabel', ...
    {'1711', '1761', '1811', '1861', ...
    '1911', '1961', '2011'})

subplot(2,1,2)
plot(t, log(mwreal)), grid
title('Results from Mann-Whitney U-test')
set(gca, 'XTickLabel', ...
    {'1711', '1761', '1811', '1861', ...
    '1911', '1961', '2011'})
legend('70 years', '140 years', '240 years')
legend('boxoff')

%%  Ansari - Bradley

for j = 1:length(w)
    df1 = w(j) - 1;
    df2 = w(j) - 1;
    for i = w(j) / 2 + 1: length(y) - w(j) / 2
        [h, p] = ansaribradley(y(i-w(j)/2:i-1), y(i + 1: i + w(j)/2));
        abreal(j,i) = p;
    end
    
    abreal(j, 1 : w(j) / 2) = abreal(j, w(j)/2 + 1) * ones(1, w(j) / 2);
    abreal(j, length(y) - w(j)/2 + 1 : length(y)) = ...
        abreal(j, length(y) - w(j) / 2) * ones(1, w(j) / 2);
end

figure
subplot(2,1,1)
plot(t, y), grid
title('Tree-Ring Based Reconstructed ENSO Index (Li et al. (2011))')
set(gca, 'XTickLabel', ...
    {'1711', '1761', '1811', '1861', ...
    '1911', '1961', '2011'})

subplot(2,1,2)
plot(t, log(abreal)), grid
title('Result from Ansari-Bradley Test')
set(gca, 'XTickLabel', ...
    {'1711', '1761', '1811', '1861', ...
    '1911', '1961', '2011'})
legend('70 years', '140 years', '240 years')
legend('boxoff')