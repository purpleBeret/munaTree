'''
NetCDF Extraction of ENSO Index from Li et al., 2011
Sandy H. <herho@terpmail.umd.edu>
12/09/2020
'''

# import libraries
import numpy as np
import pandas as pd 
import xarray as xr 

# data extraction
ds = xr.open_dataset('./data/ienso_li.nc', decode_times = False)
ei = ds['ENSO']
ei = ei.to_dataframe()['ENSO'].to_numpy()
year = np.arange(900, 2003)
df = pd.DataFrame(data = {'ENSO' : ei}, index = year)
df.index.name = 'Year'
df = df.loc[1711:1994]
df.to_csv('enso_li.csv')