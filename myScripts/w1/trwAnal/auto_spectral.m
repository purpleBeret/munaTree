%{
TRW Autospectral Analysis
Sandy. H <herho@terpmail.umd.edu>
12/09/2020
%}

clear; close all; clc

trw = normalize(detrend(readtable(".\data\tg01c_trws.csv").Var2'));
len = length(trw);
fs =1
nfft = 2^nextpow2(len)
[Pxx, f] = periodogram(trw, [], nfft, 1/fs);
plot(f, Pxx), grid;
axis([0 .5 0 max(Pxx)])
xlabel('Frequency (yr^{-1})')
ylabel('Power')
