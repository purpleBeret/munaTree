%{
Continuous Wavelet Transform (CWT), 
Cross-Wavelet Transform (XWT),
Wavelet Coherence Tranform (WTC)
for TRW tg01c using Grinsted et al.(2004) Algorithm

Sandy H. <herho@terpmail.umd.edu>
12/09/2020
%}

clear all; close all; clc

%% Set Path
addpath('wavelet-coherence-master');

%% Read data
seriesname = {'ENSO' 'TRW'};
d1 = readtable('.\data\enso_li.csv');
d2 = readtable('.\data\tg01c_trws.csv');
d1 = normalize(detrend(d1.ENSO));
d2 = normalize(detrend(d2.Var2));

%% Change series to pdf
d1 = boxpdf(d1);
d2 = boxpdf(d2);

%% Continuous Wavelet
figure('color',[1 1 1])
subplot(2,1,1);
wt(d1);
title(seriesname{1});
set(gca, 'XTickLabel', ...
    {'1761', '1811', ...
    '1861', '1911', '1961'})
colormap('hot')

subplot(2,1,2)
wt(d2)
title(seriesname{2})
set(gca, 'XTickLabel', ...
    {'1761', '1811', ...
    '1861', '1911', '1961'})
colormap('hot')

%% Cross Wavelet
figure('color',[1 1 1])
xwt(d1,d2)
title(['XWT: ' seriesname{1} '-' seriesname{2} ] )
set(gca, 'XTickLabel', ...
    {'1761', '1811', ...
    '1861', '1911', '1961'})
colormap('hot')

%% Wavelet Coherence
figure('color',[1 1 1])
wtc(d1,d2)
colormap('hot')
ylabel('Period (year)')
set(gca, 'XTickLabel', ...
    {'1761', '1811', '1861', ...
    '1911', '1961'})