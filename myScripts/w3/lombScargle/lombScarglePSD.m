%{
Detecting Power Spectral Density in 
O16 & C13 from tg01c without interpolation
using Lomb-Scargle Algorithm

Sandy H. <herho@terpmail.umd.edu>
20/01/2021
%}

clear; close all; clc

%% data preprocess
data = readtable('data_clean.csv');
now = datetime('2021-01-01');
date = datetime(data.date);
duration = years(now - date)'; % years before Jan 1st 2021

% primary data
o18 = flip(data.dO18');
c13 = flip(data.dC13');
t = flip(duration);

%% O18
int = mean(diff(t));
ofac = 4; hifac = 1;
f = ((2 * int)^(-1)) / (length(o18) * ofac):...
    ((2 * int)^(-1)) / (length(o18) * ofac):...
    hifac * (2 * int)^(-1);

o18 = o18 - mean(o18);

for k = 1 : length(f)
    wrun = 2 * pi * f(k);
    px(k) = 1 / (2 * var(o18)) * ...
        ((sum(o18 .* cos(wrun * t - ...
        atan2(sum(sin(2 * wrun * t)), sum(cos(2 * wrun * t))) /2 ))) .^ 2)...
        / (sum((cos(wrun * t - ...
        atan2(sum(sin(2 * wrun * t)), sum(cos(2 * wrun * t)))/2)) .^ 2)) + ...
        ((sum(o18 .* sin(wrun*t - ...
        atan2(sum(sin(2 * wrun * t)), sum(cos(2 * wrun * t)))/2)) .^ 2));
end

prob = 1 - (1 - exp(-px)) .^ (2 * length(o18));

figure(1)
plot(f, px), grid
xlabel('Frequency')
ylabel('Power')
title('Lomb-Scargle Power Spectrum of \delta^{18}O')

figure(2)
plot(f, prob), grid
xlabel('Frequency')
ylabel('Probability')
title('False-Alarm Probabilities of \delta^{18}O')

%% C13
int = mean(diff(t));
ofac = 4; hifac = 1;
f = ((2 * int)^(-1)) / (length(o18) * ofac):...
    ((2 * int)^(-1)) / (length(o18) * ofac):...
    hifac * (2 * int)^(-1);

c13 = c13 - mean(o18);

for k = 1 : length(f)
    wrun = 2 * pi * f(k);
    px(k) = 1 / (2 * var(c13)) * ...
        ((sum(c13 .* cos(wrun * t - ...
        atan2(sum(sin(2 * wrun * t)), sum(cos(2 * wrun * t))) /2 ))) .^ 2)...
        / (sum((cos(wrun * t - ...
        atan2(sum(sin(2 * wrun * t)), sum(cos(2 * wrun * t)))/2)) .^ 2)) + ...
        ((sum(c13 .* sin(wrun*t - ...
        atan2(sum(sin(2 * wrun * t)), sum(cos(2 * wrun * t)))/2)) .^ 2));
end

prob = 1 - (1 - exp(-px)) .^ (2 * length(c13));

figure(3)
plot(f, px), grid
xlabel('Frequency')
ylabel('Power')
title('Lomb-Scargle Power Spectrum of \delta^{13}C')

figure(4)
plot(f, prob), grid
xlabel('Frequency')
ylabel('Probability')
title('False-Alarm Probabilities of \delta^{13}C')