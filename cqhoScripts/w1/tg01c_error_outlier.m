% Last edited by Cristy Ho on 12/17/20
% Task 1: Calculate the error for each run and add error bars to samples.
% Task 2: Find outliers in the delta18O measurements for each year.

tg01c = readtable('organized_tg01c_isotope_data.xlsx');
% Adds spreadsheet to workspace

year = tg01c{:,13};
o18c = tg01c{:,10};
c13c = tg01c{:,9};
o18ccstdSAC = tg01c{:,16};
o18ccstdAKC = tg01c{:,18};
% Identify the columns for year, o18c, c13c, o18ccstdSAC, o18ccstdAKC

TF1 = isoutlier(o18c)
% Find outliers in the delta18O measurements for each year
%By default, an outlier is a value that is more than three scaled median
%abosolute deviations away from the median. Could also try doing
%isoutlier(A,method), with method being mean (so it would calculate 
%three standard deviations from the mean)


err=sqrt((o18ccstdSAC.^2)/12+(o18ccstdAKC.^2)/12);
disp(err)
% Calculate error using oxygen precisions for both SAC and AKC

figure;
nexttile % So two plots are side by side in one figure

plot(year,o18c,'o');
errorbar(year,o18c,err,'o','MarkerSize',2,...
    'MarkerEdgeColor','black','MarkerFaceColor','black')
hold on
plot(year(TF1),o18c(TF1),'x'); % x on delta18o outliers
title('\delta^{18}O for each year')
xlabel('year')
ylabel('\delta^{18}O (per mil, VSMOW)')
grid on
legend('O18','delta18O Outlier')
hold off

nexttile % So two plots are side by side in one figure

plot(year,c13c,'o');
errorbar(year,c13c,err,'o','MarkerSize',2,...
    'MarkerEdgeColor','black','MarkerFaceColor','black')
hold on
plot(year(TF1),c13c(TF1),'x'); % x on delta18o outliers
xlabel('year')
ylabel('\delta^{14}C (per mil, VSMOW)')
title('\delta^{14}C for each year')
grid on
legend('C13','delta18O Outlier')
hold off

