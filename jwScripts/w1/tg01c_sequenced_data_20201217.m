%This script is to create a graphical view of TG01C isotope data in sequence
%within each year, and possibly later to perform analyses on variations within
%each year (though a different script might be used for that purpose).

  %It should be noted that the sequencing of samples is very rough.
  %I will be going back through older microtoming notes to determine how
  %accurately sample letters describe temporal sequence for each year, and may
  %create an updated version of this code at that time.

  
  %Script created 12/17/2020 by JCW, most recently edited 12/17/2020.

open organized_tg01c_isotope_data_sequence.dat
% (Import as a numeric matrix)

  % Column 1: Position
  % Column 2: sample_ID
  % Column 3: date_time
  % Column 4: RT
  % Column 5: amplitude
  % Column 6: mass
  % Column 7: C13
  % Column 8: O18
  % Column 9: Corrected c13 data
  % Column 10: Corrected O18 data
  % Column 11: run ID (year month day)
  % Column 12: Subsamples included in wrapped capsule
  % Column 13: year
  % Column 14: assigned letter in sample run
  % Column 15: sequence within year (starting at 0 within each year)
  % Column 16: number of sequenced samples within year
  % Column 17: sequenced date (year + (col15+0.5)/col16)


   allyears = unique(organizedtg01cisotopedatasequence(:,13));

   
   figure(1)
   plot(organizedtg01cisotopedatasequence(1:649,17),organizedtg01cisotopedatasequence(1:649,10),'x')
  % hold on
  % xline(allyears(:,:))
  % hold off
   title('\delta^1^8O data within each year')
   xlabel('year')
   ylabel('\delta^1^8O')
   grid on
   grid minor

      figure(2)
   plot(organizedtg01cisotopedatasequence(1:528,17),organizedtg01cisotopedatasequence(1:528,10),'x')
   %hold on
   %xline(allyears(:,:))
   %hold off
   title('\delta^1^8O data within each year (1743-1848)')
   xlabel('year')
     ylabel('\delta^1^8O')
   grid on
   hold on
     g_x=[1740:1:1860]; %This defines the x grid [start:spaces:end].
     g_y=[18:0.5:30];

for i=1:length(g_x)
	plot([g_x(i) g_x(i)],[g_y(1) g_y(end)],'k:')
	hold on
	end

	for i=1:length(g_y)
	plot([g_x(1) g_x(end)],[g_y(i) g_y(i)],'k:')
	hold on
	end

%The above loop places the grid lines so that there is a vertical grid line at the start of each year.

      figure(3)
   plot(organizedtg01cisotopedatasequence(531:649,17),organizedtg01cisotopedatasequence(531:649,10),'x')
  % hold on
  % xline(allyears(:,:))
  % hold off
   title('\delta^1^8O data within each year(1956-1995)')
   xlabel('year')
   ylabel('\delta^1^8O')
   grid on
   grid minor

      figure(4)
   plot(organizedtg01cisotopedatasequence(1:354,17),organizedtg01cisotopedatasequence(1:354,10),'x')
   %hold on
   %xline(allyears(:,:))
   %hold off
   title('\delta^1^8O data within each year (1743-1799)')
   xlabel('year')
     ylabel('\delta^1^8O')
   grid on
      hold on
     g_x=[1740:1:1800]; %This defines the x grid [start:spaces:end].
     g_y=[18:0.5:30];

for i=1:length(g_x)
	plot([g_x(i) g_x(i)],[g_y(1) g_y(end)],'k:')
	hold on
	end

	for i=1:length(g_y)
	plot([g_x(1) g_x(end)],[g_y(i) g_y(i)],'k:')
	hold on
	end


      figure(5)
   plot(organizedtg01cisotopedatasequence(355:528,17),organizedtg01cisotopedatasequence(355:528,10),'x')
   %hold on
   %xline(allyears(:,:))
   %hold off
   title('\delta^1^8O data within each year (1800-1848)')
   xlabel('year')
     ylabel('\delta^1^8O')
   grid on
      hold on
     g_x=[1800:1:1860]; %This defines the x grid [start:spaces:end].
     g_y=[18:0.5:30];

for i=1:length(g_x)
	plot([g_x(i) g_x(i)],[g_y(1) g_y(end)],'k:')
	hold on
	end

	for i=1:length(g_y)
	plot([g_x(1) g_x(end)],[g_y(i) g_y(i)],'k:')
	hold on
	end

